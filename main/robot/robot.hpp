#ifndef ROBOT_HPP
#define ROBOT_HPP

class robot {
public:
    /* ------ SETTERS ------ */
    // Motores
    virtual void set_velocity_M1(int vel) = 0;
    virtual void set_velocity_M2(int vel) = 0;
    virtual void set_velocity(int v1, int v2) = 0;

    // Leds en la placa
    virtual void setRGB(int led, int r, int g, int b) = 0;

    
    /* ------ GETTERS ------- */
    // Motores
    virtual int get_velocity_M1() const = 0;
    virtual int get_velocity_M2() const = 0;
    virtual int get_max_velocity() const = 0;
    virtual int get_min_velocity() const = 0;

    // Sensor de linea
    virtual bool has_left_line() const = 0;
    virtual bool has_right_line() const = 0;

    // Sensor ultrasonido
    virtual float get_distance_cm() const = 0;

    // Boton en la placa
    virtual bool button_is_pressed() const = 0;
};

#endif
