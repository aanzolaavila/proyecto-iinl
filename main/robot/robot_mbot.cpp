#ifndef ROBOT_MBOT_CPP
#define ROBOT_MBOT_CPP

#include "robot.hpp"
#include "MeMCore.h"
#include <math.h>

class mbot : public robot {
public:
    mbot(MeUltrasonicSensor *us,
	 MeLineFollower *lf,
	 MeDCMotor *m1,
	 MeDCMotor *m2,
	 MeRGBLed *l,
	 int but_pin) {
	ultraSensor = us;
	lineFinder = lf;
	motor1 = m1;
	motor2 = m2;
	leds = l;
	button_pin = but_pin;
    }

    void set_velocity_M1(int);
    void set_velocity_M2(int);
    void set_velocity(int, int);
    void setRGB(int, int, int, int);

    int get_velocity_M1() const;
    int get_velocity_M2() const;
    int get_max_velocity() const;
    int get_min_velocity() const;

    bool has_left_line() const;
    bool has_right_line() const;

    float get_distance_cm() const;

    bool button_is_pressed() const;
    
private:
    const int max_velocity = 255;
    const int min_velocity = 75;
    
    MeUltrasonicSensor *ultraSensor;
    MeLineFollower *lineFinder;
    MeDCMotor *motor1;
    int m1_velocity = 0;
    MeDCMotor *motor2;
    MeRGBLed *leds;
    int m2_velocity = 0;
    int button_pin;
};

void mbot::set_velocity_M1(int vel) {
    m1_velocity = max(-max_velocity, min(max_velocity, vel));
    motor1->run(-m1_velocity); // velocidad negativa(esta al reves)
}

void mbot::set_velocity_M2(int vel) {
    m2_velocity = max(-max_velocity, min(max_velocity, vel));
    motor2->run(m2_velocity);
}
    
void mbot::set_velocity(int v1, int v2) {
    set_velocity_M1(v1);
    set_velocity_M2(v2);
}

void mbot::setRGB(int led, int r, int g, int b) {
    r = min(255, max(0, r));
    g = min(255, max(0, g));
    b = min(255, max(0, b));
    switch(led % 3) {
    case 0: // ambos
	leds->setColor(0,r,g,b);
	break;
    case 1: // izquierdo
	leds->setColor(2,r,g,b);
	break;
    case 2: // derecho
	leds->setColor(1,r,g,b);
	break;
    }
    
    leds->show();
}

int mbot::get_velocity_M1() const {
    return m1_velocity; 
}

int mbot::get_velocity_M2() const {
    return m2_velocity;
}

int mbot::get_max_velocity() const {
    return max_velocity;
}

int mbot::get_min_velocity() const {
    return min_velocity;
}

bool mbot::has_left_line() const {
    int state = lineFinder->readSensors();
    bool r = false;
    switch(state) {
    case S1_IN_S2_IN: 
    case S1_IN_S2_OUT: r = true;
    default: break;
    }

    return r;
}

bool mbot::has_right_line() const {
    int state = lineFinder->readSensors();
    bool r = false;
    switch(state) {
    case S1_OUT_S2_IN:
    case S1_IN_S2_IN: r = true;
    default: break;
    }

    return r;
}

float mbot::get_distance_cm() const {
    return ultraSensor->distanceCm();
}

bool mbot::button_is_pressed() const {
    return 0^(analogRead(button_pin)>10? 0 : 1);
}

#endif
