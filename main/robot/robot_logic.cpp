#ifndef ROBOT_LOGIC_CPP
#define ROBOT_LOGIC_CPP

#include "robot.hpp"
#include "logger.cpp"
#include <Arduino.h>
#include <math.h>

#define MINIMUM_DISTANCE 35.0
#define STOP_TIME 30
#define ROTATE_VELOCITY 100
#define ACCELERATION 150
#define DELTA_VELOCITY 5
#define INRANGE(i, a, b) ((a) <= (i) && (i) <= (b))

#define CHAR_SIZE 50

inline bool in_range(int i, int a, int b) {
    return INRANGE(i, a, b);
}

inline int range(int i, int a, int b) {
    return max(a, min(i, b));
}

typedef unsigned long time_t;

enum LOCATION{LEFT = 0, RIGHT};

class rlogic {
public:
    rlogic(robot *b, logger *l, time_t u) {
	bot = b;
	log = l;
	unit = u;
	timeout = 0;
	stop_time = 0;
	last_known_location = LEFT;
	velocity = 0;
	acc = ACCELERATION;
	m1vel = 0;
	m2vel = 0;
	init();
    }

    void update(time_t delta);

private:
    robot *bot;

    bool is_paused = true;
    bool first_execution = true;
    bool is_pressed = false;
    time_t delta;
    logger *log;
    double unit;
    time_t timeout;
    time_t stop_time;
    bool out_of_line = true;
    int last_known_location;
    int velocity;
    double acc; // aceleracion
    double m1vel;
    double m2vel;

    double m1target = 0;
    double m2target = 0;

    char tmp[CHAR_SIZE];
    char ntmp[CHAR_SIZE];

    void init();
    void execute();
    void conv(double);
    void move();
    void move(int, int);
};

void rlogic::init() {
    bot->setRGB(0, 0, 0, 255); // azul
    log->info("se inicia logica");
}

void rlogic::conv(double v) {
    dtostrf(v, 4, 2, ntmp);
}

void rlogic::update(time_t dt) {
    delta = dt;

    if (is_pressed) {
	log->debug("el boton sigue oprimido");
    }

    bool pressed;
    
    if ((pressed = bot->button_is_pressed()) && !is_pressed ) {
	log->info("se oprime el boton de la placa");
	is_paused ^= 1;
	if (first_execution) {
	    first_execution = false;
	    log->info("se inicia por primera vez");
	}
	
	if (! is_paused){
	    bot->setRGB(0, 0, 255, 0); // verde
	    log->info("resumido");
	} else {
	    log->info("pausado");
	}
	
	is_pressed = true;
    } else if (!pressed && is_pressed) {
	is_pressed = false;
    }

    if (!first_execution && !is_pressed) {
	if (!is_paused) {
	    execute();
	} else {
	    bot->setRGB(0, 255, 0, 0); // rojo
	    //bot->set_velocity(0, 0);
	    move(0, 0);
	}
    }

    move();
}

void rlogic::execute() {
    conv(bot->get_distance_cm());
    sprintf(tmp, "distancia sensor: %s", ntmp);
    log->debug(tmp);
    if (bot->get_distance_cm() <= MINIMUM_DISTANCE || true) { // XXX
	log->info("se detecta un objeto");
	bool left = bot->has_left_line();
	bool right = bot->has_right_line();
	out_of_line = !(left || right);
	if (out_of_line) {
	    log->error("no esta sobre la linea, retrocediendo");
	    bot->setRGB(0, 255, 0, 255);
	    velocity = bot->get_max_velocity();
	    
	    //bot->set_velocity(m1vel, m2vel);
	    move(-velocity, -velocity);
	} else {
	    int lvel = bot->get_min_velocity();
	    int rvel = bot->get_min_velocity();

	    sprintf(tmp, "linea IZ: %d DE: %d", left, right);
	    log->debug(tmp);
	    
	    if (left) {
		bot->setRGB(1, 0, 255, 0);
		lvel = 0;
	    } else {
		bot->setRGB(1, 255, 0, 255);
		lvel = bot->get_max_velocity();
	    }

	    if (right) {
		bot->setRGB(2, 0, 255, 0);
		rvel = 0;
	    } else {
		bot->setRGB(2, 255, 0, 255);
		rvel = bot->get_max_velocity();
	    }

	    if (left && right) {
		bot->setRGB(0, 0, 255, 0);
		lvel = rvel = bot->get_max_velocity();
	    }

	    //bot->set_velocity((int)lvel, (int)rvel);
	    move(lvel, rvel);
	}
    } else {
	log->info("no hay objeto encima, pausando");
	is_paused = true;
    }

    sprintf(tmp, "Velocidades Motor: L: %d R: %d | logica: L: %d R: %d",
	    bot->get_velocity_M1(),
	    bot->get_velocity_M2(),
	    (int)m1vel,
	    (int)m2vel);
    log->debug(tmp);
}

void rlogic::move(int v1, int v2) {
    int v = bot->get_max_velocity();
    m1target = range(v1, -v, v);
    m2target = range(v2, -v, v);
}

void rlogic::move() {
    double a = acc / unit * delta;
    /*conv(a);
    sprintf(tmp, "a = %s", ntmp);
    log->debug(tmp);*/
    m1vel += copysign(a, m1target - bot->get_velocity_M1());
    m2vel += copysign(a, m2target - bot->get_velocity_M2());

    m1vel = abs(m1vel - m1target) <= DELTA_VELOCITY ?
	m1target : m1vel;

    m2vel = abs(m2vel - m2target) <= DELTA_VELOCITY ?
	m2target : m2vel;

    int mx = bot->get_max_velocity();
    m1vel = range(m1vel, -mx, mx);
    m2vel = range(m2vel, -mx, mx);
    
    bot->set_velocity(m1vel, m2vel);
}

#endif
